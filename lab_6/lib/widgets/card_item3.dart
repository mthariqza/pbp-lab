import 'package:flutter/material.dart';


class CardItem3 extends StatefulWidget {
  const CardItem3 ({Key? key}) : super(key: key);

  @override
  _CardItem3State createState() => _CardItem3State();
}

class _CardItem3State extends State<CardItem3> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        color: Colors.amber[100],
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          child: SizedBox(
            width: 300,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child:
                  Column(
                      children: [
                        SizedBox(height: 10,),
                        Text('3. Mencuci Tangan',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center,),
                        SizedBox(height: 20,),
                        Text("Mencuci tangan dengan benar atau menggunakan hand sanitizer adalah cara paling sederhana namun efektif untuk mencegah penyebaran virus 2019-nCoV.", style: TextStyle(
                          fontSize: 15,
                        ),
                          textAlign: TextAlign.center,),
                      ]
                  ),),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
