import 'package:flutter/material.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({Key? key}) : super(key: key);

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            decoration: BoxDecoration(
              color: Colors.teal[600],
            ),
            accountName: Text("Hi M Thariq Zahir!"),
            accountEmail: Text("mthariqza@gmail.com"),
            currentAccountPicture: CircleAvatar(
              backgroundColor: Colors.teal[600],
            ),
          ),
          DrawerListTile(
            iconData: Icons.question_answer_outlined,
            title: "FUFU",
            onTilePressed: () {
            },
          ),
          DrawerListTile(
            iconData: Icons.local_hospital_outlined,
            title: "Rumah Sakit",
            onTilePressed: () {
            },
          ),
          DrawerListTile(
            iconData: Icons.quiz,
            title: "FaQ",
            onTilePressed: () {
            },
          ),
          DrawerListTile(
            iconData: Icons.query_stats,
            title: "Mitos/Fakta",
            onTilePressed: () {
            },
          ),
          DrawerListTile(
            iconData: Icons.gradient_sharp,
            title: "Infografis",
            onTilePressed: () {
            },
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData ;
  final String title;
  final VoidCallback onTilePressed;
  const DrawerListTile({Key? key, required this.iconData, required this.title, required this.onTilePressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(title, style: TextStyle(fontSize: 16),),
    );
  }
}
