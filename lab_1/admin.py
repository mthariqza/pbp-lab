from lab_1.models import Friend
from django.contrib import admin
from django.contrib.admin.decorators import register
from .models import Friend

admin.site.register(Friend)
# TODO Register Friend model here
