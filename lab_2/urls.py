from django.urls import path
from .views import index
from .views import json, xml

urlpatterns = [
    path('', index, name='index'),
    path('xml', xml),
    path('json', json)
    # TODO Add friends path using friend_list Views
]
