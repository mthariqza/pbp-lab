1. Perbedaan antara JSON dan XML
XML dan JSON adalah dua format paling umum untuk pertukaran data. JSON hanyalah format data sedangkan XML adalah bahasa markup. JSON digunakan untuk menyimpan informasi dengan cara yang terorganisir dan mudah diakses. Bentuk lengkapnya adalah JavaScript Object Notation. Ini menawarkan kumpulan data yang dapat dibaca manusia yang dapat diakses secara logis. Ekstensi file JSON adalah .json. XML adalah bahasa markup yang dirancang untuk menyimpan data. Ini populer digunakan atau transfer data. Case sensitive pada huruf besar/kecil. XML menawarkan kalian untuk menentukan elemen markup dan menghasilkan bahasa markup yang disesuaikan. Unit dasar dalam XML dikenal sebagai elemen. Ekstensi file XML adalah .xml. XML digunakan untuk menyimpan dan mengangkut data dari satu aplikasi ke aplikasi lain melalui Internet. JSON di sisi lain adalah format pertukaran data ringan yang jauh lebih mudah bagi komputer untuk mengurai data yang sedang dikirim. Format JSON menggambarkan diri sendiri dan secara komparatif jauh lebih mudah dibaca daripada dokumen berformat XML. Namun, jika sebuah proyek memerlukan markup dokumen dan informasi metadata, lebih baik menggunakan XML.


2. Perbedaan HTML dan XML 
HTML (HyperText Markup Language) digunakan untuk membuat halaman web dan aplikasi web. Ini adalah bahasa komputer yang digunakan untuk menerapkan tata letak dan konvensi pemformatan ke dokumen teks. XML (eXtensible Markup Language) juga digunakan untuk membuat halaman web dan aplikasi web. Tetapi ini adalah bahasa dinamis yang digunakan untuk mengangkut data dan bukan untuk menampilkan data. Tujuan desain XML berfokus pada kesederhanaan, umum, dan kegunaan di Internet.

Ada beberapa parameter berbeda untuk membandingkan perbedaan antara HTML dan XML. Mari lihat daftar parameter dan perbedaan antara kedua bahasa tersebut:
1. XML berfokus pada transfer data sedangkan HTML difokuskan pada penyajian data.
2. XML didorong konten sedangkan HTML didorong oleh format.
3. XML itu Case Sensitive sedangkan XML Case Insensitive
4. XML menyediakan dukungan namespaces sementara HTML tidak menyediakan dukungan namespaces.
5. XML strict untuk tag penutup sedangkan HTML tidak strict.
6. Tag XML dapat dikembangkan sedangkan HTML memiliki tag terbatas.
7. Tag XML tidak ditentukan sebelumnya sedangkan HTML memiliki tag yang telah ditentukan sebelumnya.



