from django.contrib import admin
from .models import Note
from django.contrib.admin.decorators import register

admin.site.register(Note)
# Register your models here.
