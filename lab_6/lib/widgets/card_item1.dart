import 'package:flutter/material.dart';


class CardItem1 extends StatefulWidget {
  const CardItem1 ({Key? key}) : super(key: key);

  @override
  _CardItem1State createState() => _CardItem1State();
}

class _CardItem1State extends State<CardItem1> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        color: Colors.amber[100],
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          child: SizedBox(
            width: 300,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child:
                  Column(
                      children: [
                        SizedBox(height: 10,),
                        Text('1. Memakai Masker',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center,),
                        SizedBox(height: 20,),
                        Text("Banyak yang menggunakan masker kain untuk mencegah infeksi virus Corona, padahal masker tersebut belum tentu efektif.", style: TextStyle(
                          fontSize: 15,
                        ),
                          textAlign: TextAlign.center,),
                      ]
                  ),),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
