import 'package:flutter/material.dart';
import './screens/homepage_screen.dart';
import './screens/drawer_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Zona Hijau',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        // home: CategoriesScreen(),
        home: const MyHome(title: 'Infografis'),
        routes: <String, WidgetBuilder>{
          '/main': (context) => const Home(),
        }

    );
  }

}
class MyHome extends StatefulWidget {
  const MyHome({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHome> createState() => _MyHomeState();
}

//
class _MyHomeState extends State<MyHome> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(widget.title),
          backgroundColor: Colors.teal[600],
        ),
        drawer: const DrawerScreen(),
        body: const SingleChildScrollView(
            child: Home()
        )
    );
  }
}



