from django.urls import path
from .views import add_note, index, note_list

urlpatterns = [
    path('', index, name='index'),
    path('add-note', add_note),
    path('note-list', note_list),
    # TODO Add friends path using friend_list Views
]
