import 'package:flutter/material.dart';


class CardItem2 extends StatefulWidget {
  const CardItem2 ({Key? key}) : super(key: key);

  @override
  _CardItem2State createState() => _CardItem2State();
}

class _CardItem2State extends State<CardItem2> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        color: Colors.amber[100],
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          child: SizedBox(
            width: 300,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child:
                  Column(
                      children: [
                        SizedBox(height: 10,),
                        Text('2. Melakukan Vaksinasi',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center,),
                        SizedBox(height: 20,),
                        Text("Pemberian vaksin ini merupakan solusi yang dianggap paling tepat untuk mengurangi dan memutus rantai penularan Covid-19.", style: TextStyle(
                          fontSize: 15,
                        ),
                          textAlign: TextAlign.center,),
                      ]
                  ),),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
