import 'package:flutter/material.dart';
import 'package:lab_6/widgets/card_item4.dart';
import 'package:lab_6/widgets/card_item5.dart';
import '../widgets/card_item1.dart';
import '../widgets/card_item2.dart';
import '../widgets/card_item3.dart';
import '../widgets/card_item4.dart';
import '../widgets/card_item5.dart';
import '../widgets/card_item6.dart';

class Home extends StatefulWidget {

  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget> [
        SizedBox(height: 50),
        CardItem1(),
        CardItem2(),
        CardItem3(),
        CardItem4(),
        CardItem5(),
        CardItem6(),
        SizedBox(height: 20),
      ],
    );
  }
}
